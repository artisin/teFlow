var teFlow = require('../../lib/te-flow');




// debugger



// var one = function  () {
//   return 1;
// };

// var two = function (oneVal) {
//   debugger
//   //oneVal is the value that was returned
//   //from the one function
//   oneVal++;
//   return arguments;
// };

// var three = function (oneVal, twoval) {
//   //oneVal === 2
//   //twoVal === 2
//   oneVal = oneVal + 3;
//   twoval = twoval - 3;
//   return arguments;
// };

// var four = function () {
//   //args === [5, -1]
//   var args = [].slice.call(arguments);
//   //only return positive vals
//   return args.reduce(function (val) {
//     if (val > 0) {
//       return val;
//     }
//   });
// };

// var res = teFlow(
//   one,
//   two,
//   three,
//   four
// );

// // res === [5]

// debugger


// var initArgs = function () {
//   debugger
//   return {
//     hmm: arguments[0]
//   }
// }

// var one = function () {
//   debugger
// };

// var two = function () {
  
// }


// var res = teFlow(
//   {
//     _args: {
//       first: 'one',
//       second: 2
//     }
//   },
//   initArgs,
//   one
// )
// 
// 

// var addNewObjArg = function (args, newObj) {
//   return Object.keys(args).reduce(function (prv, cur) {
//     var curObj = args[cur],
//         curKey = Object.keys(curObj)[0];
//     prv[curKey] = curObj[curKey];
//     return prv;
//   }, newObj);
// };

// var one = function () {
//   return {
//     keyOne: 1
//   };
// };
// var two = function (oneVal) {
//   //oneVal {keyOne: 1}
//   return addNewObjArg(arguments, {keyTwo: 2});
// };
// var three = function (oneVal, twoVal) {
//   //oneVal {keyOne: 1}
//   //twoVal {keyTwo: 2}
//   return addNewObjArg(arguments, {keyThree: 3});
// };


// var merge = function() {
//     var obj = {},
//         i = 0,
//         il = arguments.length,
//         key;
//     for (; i < il; i++) {
//         for (key in arguments[i]) {
//             if (arguments[i].hasOwnProperty(key)) {
//                 obj[key] = arguments[i][key];
//             }
//         }
//     }
//     return obj;
// };



// var one = function () {
//   return {
//     keyOne: 1
//   };
// };
// var two = function (oneVal) {
//   //oneVal {keyOne: 1}
//   return merge(arguments, {keyTwo: 2});
// };
// var three = function (oneVal, twoVal) {
//   //oneVal {keyOne: 1}
//   //twoVal {keyTwo: 2}
//   return merge(arguments, {keyThree: 3});
// };


// var res = teFlow(
//     {
//       _objKeep: true
//     },
//     one,
//     two,
//     three
// );

// res === [1, 2, 4];


// var addNewObjArg = function (args, newObj) {
//   // args.push(newObj);
//   return Object.keys(newObj).reduce(function (prv, curKey) {
//     debugger
//     prv[curKey] = newObj[curKey];
//     return prv;
//   }, args);
// };

var beThis = (function () {
  var count = 0;
  var cool = function (obj) {
    this.name = obj.name;
    this.getName = function () {
      return this.name;
    };
    this.changeName = function (newName) {
      this.name = newName;
    };
    this.returnThis = function () {
      return this;
    };
    this.incNum = function () {
      count++;
    };
    this.rtnNum = function() {
      return count;
    };
  };
  return cool;
}());

var addMe = function () {
  return this.getName();
};

var changeMe = function (name) {
  //name === '</artisin>' 
  //bump shared count 
  this.incNum();
  //change name 
  this.changeName('Te');
  return {
    oldName: name,
    newName: this.getName()
  };
};
 
var addYou = function (oldName, newName) {
  //oldName === '</artisin>' 
  //newName === 'Te' 
  //Add new beThis
  var you = new beThis({
    name: 'You'
  });
  //bind me to current this ref for latter use
  var me = (function() {
    return this;
  }.bind(this)());
  return {
    //reassign this 
    _this: you,
    me: me
  };
};
 
var res = teFlow(
    {
      //set init this 
      _this: new beThis({
        name: '</artisin>'
      })
    },
    addMe,
    changeMe,
    addYou,
    {
      return: function (me) {
        //me.oldName === '</artisin>' 
        //me.name === 'Te' 
        debugger
        return {
          count: this.rtnNum(),
          myName: me.getName(),
          //reassigned this from prv fn 
          yourName: this.getName()
        };
      }
    }
);


debugger